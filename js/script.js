document.addEventListener('DOMContentLoaded', onReady);

function onReady () {
    let topBtn = document.getElementById('topBtn');
    topBtn.addEventListener('click', onTopBtnClick);
    let closeBtn = document.getElementById('closeBtn');
    closeBtn.addEventListener('click', onCloseBtnClick);
}

function onTopBtnClick () {
    let modal = document.getElementById('modal');
    modal.classList.add ('active');
}

function onCloseBtnClick () {
    let modal = document.getElementById('modal');
    modal.classList.remove('active');
}



