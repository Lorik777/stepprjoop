class Visit {
    constructor (name, date, id, issue, age) {
        this.name = name;
        this.date = date;
        this.id = id;
        this.issue = issue;
        this.age = age;
    }
    build (elem) {
        return document.createElement(elem);
    }
}

class Cardiologist extends Visit {
    constructor (pressure, weigh, disease) {
        super(issue);
        super(id);
        super(age);
        this.pressure = pressure;
        this.weigh = weigh;
        this.disease = disease;
    }
    build () {
        return super.build ('div');
    }
}

class Therapist extends Visit {
    constructor () {
        super(issue);
        super(id);
        super(age);
    }
    build () {
        return super.build ('div');
    }
}

class Dantist extends Visit {
    constructor (lastVisitDate) {
        super(issue);
        super(id);
        this.lastVisitDate = lastVisitDate;
    }
    build () {
        return super.build ('div');
    }
}